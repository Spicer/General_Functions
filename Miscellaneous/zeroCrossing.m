function [indx_up, indx_down] = zeroCrossing(signal)
%             x = diff(sign(signal));
%             indx_up = find(x>0);
%             indx_down = find(x<0);
        ind = signal > (mean((signal)));%+0.01*std(signal));
        indx_up = find(diff(ind) > 0);
        indx_down = find(diff(ind) < 0);
end