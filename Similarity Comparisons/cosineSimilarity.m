function cos_sim = cosineSimilarity(x1, x2)

cos_sim = dot(abs(x1), abs(x2))/(sqrt(dot(x1,x1))*sqrt(dot(x2,x2)));

end