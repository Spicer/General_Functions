function jsim = jaccardSim(x_vec, y_vec)
%Jaccard Similarity
% This function takes two vectors (currently ONLY VECTORS), and calculates
% the Jaccard similarity index for real vectors x, y >= 0, as defined here:
% https://en.wikipedia.org/wiki/Jaccard_index
% If x, and y contain negative arguments, the absolute value is taken.
%
% Input arguments:
% x_vec is the one of the vectors to be compared
% y_vec is the one of the vectors to be compared


sz1 = size(x_vec);
sz2 = size(y_vec);

% if sz1(1) < sz1(2)
%     x_vec = x_vec.';
% end
% if sz2(1) < sz2(2)
%     y_vec = y_vec.';
% end
if sz1(2) > 1 && sz2(2) > 1
    matty = cat(3, x_vec, y_vec);
    minnie = min(abs(matty), [], 3);
    maxy = max(abs(matty), [], 3);

    jsim = sum(minnie, 2) ./ sum(maxy, 2);
else
    
    matty = [x_vec, y_vec];
    minnie = abs(min(matty, [], 2));
    maxy = abs(max(matty, [], 2));

    jsim = sum(minnie) / sum(maxy);
    
    if jsim > 1
        minnie = min(abs(matty), [], 2);
        maxy = max(abs(matty), [], 2);
        jsim = sum(minnie) / sum(maxy);
end

end