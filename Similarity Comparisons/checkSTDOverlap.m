function overlap = checkSTDOverlap(dat_a, dat_b)
% dat_a should be a matrix where the rows are RVs and each column
% represents an observation. dat_b may be a matrix or a vector. If dat_b is
% a vector, it is checked to see if it falls within 95% confidence of dat_a
% (2 standard deviations). If dat_b is a matrix, 95% CIs are found to each,
% and the amount of overlap is measured. If dat_b is a matrix, overlap
% measures a percentage of overlap (if there is no overlap, then overlap
% is 0). If dat_b is a vector, it measures how many standard deviations
% away from the mean of dat_a each point is.

mean_a = mean(dat_a, 2); std_a = std(dat_a, 0, 2);
if size(dat_b, 2) > 1
    mean_b = mean(dat_b, 2); std_b = std(dat_b, 0, 2);
    CI_a = [mean_a-2*std_a, mean_a+2*std_a];
    CI_b = [mean_b-2*std_b, mean_b+2*std_b];
    overlap = jaccardSim(CI_a, CI_b);
else
    overlap = (dat_b - mean_a) ./ std_a;
end

end