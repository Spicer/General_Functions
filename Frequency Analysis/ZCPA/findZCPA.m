function [freqs, amps] = findZCPA(signal, sample_rate, cutoffs)

signal = signal - mean(signal);
[ind_up, ind_down] = zeroCrossing(signal);
schmitt_trigger = zeros(size(signal, 1), 1);
schmitt_trigger(ind_up) = 1;
schmitt_trigger(ind_down) = -1;
% schmitt_trigger = zeros(size(signal, 1), 1);
% schmitt_trigger(indx_up) = 1;
% schmitt_trigger(indx_down) = -1;
% figure(); plot(signal); hold on; plot(schmitt_trigger)

t_vec = linspace(0, (length(signal)-1)/sample_rate, length(signal)).';
% ind_up = find(ind_up);
freqs = 1 ./ diff(t_vec(ind_up));
amps = zeros(length(freqs), 1);

keepers = find(freqs>(0.9*cutoffs(1)) & freqs<(cutoffs(2)*1.1));
freqs = freqs(keepers); amps = amps(keepers);

for ii = 1:length(freqs)
    amps(ii) = max(abs(signal(ind_up(ii):ind_up(ii+1))));
end
amps = log(1+amps);

end