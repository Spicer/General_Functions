function [cep, binned_spec, bin_centers, specgram] = zcpa(data, ...
    sample_rate, num_cep, app, num_spec)

%% Use the filter bank
[filt_sigs, filt_freqs, erb, cutoffs] = filterBank(data, sample_rate, app);
num_chan = size(filt_sigs, 2);

% figure(); hold on; plot(filt_sigs(:,5)); plot(filt_sigs(:,12)); legend('low freq', 'high freq')
% [fl, ml] = posFFT(filt_sigs(:,5), sample_rate, 2^13);
% [fh, mh] = posFFT(filt_sigs(:,12), sample_rate, 2^13);
% figure(); hold on; plot(fl, abs(ml)); plot(fh, abs(mh)); legend('low freq', 'high freq')
%% Break up the data into frames and do the zpca
frame_len = 10 ./ filt_freqs; %length in seconds
frame_ind_len = frame_len * sample_rate;
num_frames = ceil(max([size(data, 1) - frame_ind_len, zeros(length(frame_ind_len), 2)]) ./ ...
    frame_ind_len(num_chan));
frame_ind_len = ceil(frame_ind_len);
iters = ceil(size(data, 1) ./ frame_ind_len);
% res = struct();
% freq = []; amp = [];
freq = zeros(sum(iters), 1);
amp = zeros(sum(iters), 1);
t_cent = zeros(max(iters), num_chan);
spectro = struct();
itt = 0;
for chan = 1:num_chan
%     sig = filt_sigs(:, chan);
%     [fr, am] = findZCPA(sig, sample_rate, cutoffs(chan, :));
%     freq = [freq; fr]; amp = [amp; am];
%     finished = false;% it=0;
    frr = -ones(11*iters(chan), 1); amm = zeros(11*iters(chan), 1);
    cuts = cutoffs(chan, :);
    for it = 1:iters(chan)
%     while ~finished
        itt=itt+1;% it=it+1;
        
        start = (it-1)*frame_ind_len(num_chan)+1;
        if start+frame_ind_len(chan) > size(filt_sigs, 1)
            t_cent(it, chan) = (start+size(filt_sigs, 1))/(2*sample_rate);
            sig = filt_sigs(start:end, chan);
%             finished = true;
        else
            t_cent(it, chan) = (start+start+frame_ind_len(chan))/(2*sample_rate);
            sig = filt_sigs(start:start+frame_ind_len(chan), chan);
        end
%         res.(['chan', num2str(chan)]).(['zcpa', num2str(it)]) = zcpa(sig, sample_rate);
        [fr, am] = findZCPA(sig, sample_rate, cuts);
%         freq = [freq; fr]; amp = [amp; am];
%         freq(itt) = fr; amp(itt) = am;
%         spectro.(['chan', num2str(chan)]).(['freq', num2str(it)]) = fr;
%         spectro.(['chan', num2str(chan)]).(['amp', num2str(it)]) = am;
        if length(fr) > 11
            disp(length(fr))
            error('you found too many zero crossings')
        end
        if ~isempty(fr)
            place = 10*(it-1) + 1;
            pl = length(fr) - 1;
            frr(place:place+pl) = fr;
            amm(place:place+pl) = am;
%         else
%             frr(it) = -ones(10, 1);
%             amm(it) = zeros(10, 1);
        end
    end
    spectro.(['chan', num2str(chan)]).freq = frr(frr ~= -1);
    spectro.(['chan', num2str(chan)]).amp = amm(amm ~= 0);
    spectro.(['chan', num2str(chan)]).t_cent = t_cent(t_cent(:, chan)~=0);
end
% combine the results
[binned_spec, bin_centers] = binning(data, sample_rate, freq, amp, num_spec, app);

[time, freq, amp] = makeSpectro(data, sample_rate, spectro, num_spec, num_chan, app);
specgram = struct('time', time, 'freq', freq, 'amp', amp);

% figure(); plot(bin_centers, binned_spec)
% title('Spectrum from ZCPA')
% ylabel('mag')
% xlabel('frequency [Hz]')

%% Combine the channels
% for chan = 1:num_chan
%     for per = 1: length(fieldnames(res.(['chan', num2str(chan)])))
%         
%     end
% end

%% Do the dct
[cep,dctm] = spec2cep(binned_spec, num_cep, 1);
% figure(); plot(cep)

end

function [binned_spec, bin_centers] = binning(data, sr, freq, amp, num_spec, app)

[sorted_freq, sort_ind] = sort(freq);
amp = amp(sort_ind);

% lin_bin_edges = (0:sample_rate/size(data,1):sample_rate/2).';
% bark_bin_edges = barkScale(lin_bin_edges);
if strcmp(app, 'voice')
    bark_bin_edges = linspace(0, 24, floor(size(data, 1)/2)).';
    lin_bin_edges = barkScale(bark_bin_edges, 2);
    num_bins = length(bark_bin_edges) - 1;
    ind = zeros(length(freq), num_bins);
elseif strcmp(app, 'electronics')
    lin_bin_edges = linspace(0, sr/2, num_spec+1).';
%     lin_bin_edges = logspace(0, log10(sample_rate/2), floor(size(data, 1)/2)).';
    num_bins = length(lin_bin_edges) - 1;
    ind = zeros(length(freq), num_bins);
end
% amp_mat = repmat(amp, 1, num_bins);
for ii = 1:num_bins
    ind(:, ii) = sorted_freq > lin_bin_edges(ii) & sorted_freq < lin_bin_edges(ii+1);
end
binned_spec = ind.' * amp;
bin_centers = (lin_bin_edges(2:end) + lin_bin_edges(1:end-1))/2;

end

function [time, freq, amp] = makeSpectro(data, sr, spectro, num_spec, num_chan, app)

time = []; freq = []; amp = [];
for chan = 1:num_chan
    time = [time; spectro.(['chan', num2str(chan)]).t_cent];
%     for kk = 1:((length(fieldnames(spectro.(['chan', num2str(chan)])))-1)/2)
    for kk = 1:(length(spectro.(['chan', num2str(chan)]).freq))
        [binned_spec, bin_centers] = binning(data, sr, ...
            spectro.(['chan', num2str(chan)]).freq(kk), ...
            spectro.(['chan', num2str(chan)]).amp(kk), ...
            num_spec, app);
        if length(binned_spec) ~= length(bin_centers)
            binned_spec = zeros(num_spec, 1);
        end
        freq = [freq, bin_centers]; amp = [amp, binned_spec];
    end
end

[time, sort_ind] = sort(time);
freq = freq(:, sort_ind); amp = amp(:, sort_ind);

end