function [filtered_signals, filt_freqs, erb, cutoffs] = filterBank(signal, sample_rate, app)

if strcmp(app, 'voice')
    num_filts = 16;
    filt_nums = (0:num_filts-1).' / (num_filts-1);
    A = 165.4; alph = 2.1; %160
    filt_freqs = A*(10.^(alph*filt_nums)-0.8);
    erb = 24.7*(4.37*filt_freqs/1000 + 1);
    cutoffs = [filt_freqs-erb, filt_freqs+erb];
elseif strcmp(app, 'electronics')
    num_filts = 100;
%     filt_nums = (1:num_filts+1)';
%     filt_freqs = 1600*10.^(filt_nums.^(1/2));
%     erb = 3/5*diff(filt_freqs);
    filt_nums = (1:num_filts+1)';
    filt_freqs = 100*10.^(filt_nums.^(4/9));
    erb = diff(filt_freqs)/2;
    cutoffs = [filt_freqs(1:end-1)-erb, filt_freqs(1:end-1)+erb];
else
    error('Unrecognized ZCPA application')
end

corr = false;
len = size(signal, 1);
if len < 3001
    corr = true;
    signal = [signal; zeros(3001-len, 1)];
end

filtered_signals = zeros(len, num_filts);
for filfil = 1:num_filts
    if filt_freqs(filfil) + erb(filfil) > sample_rate/2
        filtered_signals = filtered_signals(:,1:filfil-1);
        filt_freqs = filt_freqs(1:filfil-1);
        break
    end
    filtered_signals(:,filfil) = filty(signal, [filt_freqs(filfil)-erb(filfil), ...
        filt_freqs(filfil)+erb(filfil)], sample_rate);
end

    function billybob = filty(sambo, cutoff, sample_rate)
%         ord = 61;
        ord = 1000;
%         ord = 1;
        buff = 2/(length(signal));
        if ~cutoff(1)
            band = cutoff(2)/(sample_rate/2);
%             b_filt = fir1(ord, cutoff(2)/(sample_rate/2), 'low');
%             b_filt = firpm(ord, [0, band, band+buff, 1], ...
%                 [1, 1, 0, 0]);
%             hd = dfilt.dffir(b_filt);
%             freqz(hd);
            a_filt=1;
%             [b_filt, a_filt] = butter(ord, cutoff(2)/(sample_rate/2), 'low');
        else
            band = cutoff/(sample_rate/2);
            b_filt = fir1(ord, cutoff/(sample_rate/2), 'bandpass');
%             hd = dfilt.dffir(b_filt);
%             freqz(hd);
%             f_filt = [0, max(band(1)-buff, 0), band, band(2)+buff, 1];
%             a_filt = [0, 0.0, 1.0, 1.0, 0.0, 0];
%             b_filt = firpm(ord, f_filt, a_filt);
%             [h,w] = freqz(b_filt,1,512);
%             figure;  plot(f_filt, a_filt, w/pi, abs(h))
%             legend('Ideal','firpm Design')
%             xlabel 'Radian Frequency (\omega/\pi)', ylabel 'Magnitude'
            a_filt=1;
%             [b_filt, a_filt] = butter(5, cutoff(2)/(sample_rate/2), 'low');
        end
        
        billybob = filtfilt(b_filt, a_filt, sambo);
        if corr
            billybob = billybob(1:len);
        end
    end

end