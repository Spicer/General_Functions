function out = barkScale(freq, dir)
%if dir=1, we go from linear to bark
%if dir=2, we go from bark to linear

if nargin < 2   dir = 1;    end

if dir == 1
    out = 6*log(freq./600 + ((freq./600).^2+1).^0.5);
elseif dir == 2
    out = 300.*exp(-freq./6) .* (exp(freq./3) - 1);
else
    error('Unrecognized direction input')
end

end