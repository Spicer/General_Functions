function [s,f,t,ps] = plotSpectrogram(data, sampling_rate)

Nx = length(data(:,1));
nsc = floor(Nx/50);
nov = floor(nsc/2);
[s,f,t,ps] = spectrogram(data(:,1),chebwin(nsc),nov,...
    nextpow2(Nx),sampling_rate);

figure()
hold on
% pcolor(t,f,abs(ps)); shading interp;
surf(t,f,abs(ps)); shading interp;
xlabel('Time (s)');
ylabel('Frequency (Hz)'); 
hold off
colorbar
% view(3)

% [q,nd] = max(10*log10(ps));

end