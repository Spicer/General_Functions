function [ref, out, diff] = phaseLockedLoop()

lf_bandwidth = 0.002;
phase_out = 0.0;
freq_out = 0.0;
vco = exp(1j*phase_out);
phase_difference = 0.0;
bw = lf_bandwidth;
beta = sqrt(lf_bandwidth);

    
num_samples = 500;
phi = 3.0;
frequency_offset = -0.2;
ref = zeros(num_samples, 1);
out = zeros(num_samples, 1);
diff = zeros(num_samples, 1);
for ii = 1:num_samples
    sig_in = exp(1j*phi);
    phi = phi + frequency_offset;
    step();
    ref(ii) = sig_in;
    out(ii) = vco;
    diff(ii) = phase_difference;
end

    function step()
        %# Takes an instantaneous sample of a signal and updates the PLL's inner state
        phase_difference = angle(sig_in*conj(vco));
        freq_out = freq_out + bw * phase_difference;
        phase_out = phase_out + beta * phase_difference + freq_out;
        vco = exp(1j*phase_out);
    end

figure()
hold on
plot(real(ref))
plot(real(out))
plot(real(diff))
legend('ref', 'out', 'diff')

end