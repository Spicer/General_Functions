function freq_array = hilbertFreqs(signal, t_vec)

t_step = t_vec(2) - t_vec(1);
hil = hilbert(signal);
angle_vec = angle(hil)* 180 / pi;
% figure()
% plot(angle_vec)
% title('angle from Hilbert Transform')

[indx_up, indx_down] = zeroCrossing(signal);
[shifted_indx_up, shifted_indx_down] = zeroCrossing(imag(hil));

    function [indx_up, indx_down] = zeroCrossing(signal)
        x = diff(sign(signal));
        indx_up = find(x>0);
        indx_down = find(x<0);
    end

mod_t_vec = t_vec(sort([indx_up; indx_down; shifted_indx_up; shifted_indx_down]));
freq_array = 4*diff(mod_t_vec)/t_step;
% freq_array = diff(indx_up)/t_step;

end