function cepstra = pcep(signal)
%PCEP Power Cepstrum
% Calculated according to https://en.wikipedia.org/wiki/Cepstrum

cepstra = abs(ifft(log(abs(fft(signal)).^2))).^2;


end