function [freqs, periodic_sig] = hilbertPeriodic(signal, t_step)

t_vec = linspace(0, t_step*length(signal), length(signal)).';

hil = hilbert(signal);
shifted_sig = imag(hil);
[shifted_zc_up, shifted_zc_down] = zeroCrossing(shifted_sig);
[zc_up, zc_down] = zeroCrossing(signal);

angle_vec = angle(hil);%* 180 / pi;
[pks, locs, ~, prom] = findpeaks(-diff(angle_vec));
ind = prom>0.5;
pks = pks(ind); locs = locs(ind);
% [pks, locs] = findpeaks(pks);
% figure(); plot(pks)
[shifted_zc_up, ~] = zeroCrossing(angle_vec);
shifted_zc_down = locs;
% shifted_zc_down = abs(angle_vec + pi) < 0.1;
% [shifted_zc_down, ~] = zeroCrossing(angle_vec + pi);
[zc_up, zc_down] = zeroCrossing(signal);
% zc_down = false(length(signal),1);
% figure()
% hold on
% plot(t_vec, angle_vec)
% plot(t_vec, signal)

periodic_sig = -2*ones(length(signal), 1);
periodic_sig(shifted_zc_up) = 1;
periodic_sig(shifted_zc_down) = -1;
periodic_sig(zc_up) = 0;
periodic_sig(zc_down) = 0;

indices = periodic_sig == 0 | periodic_sig == 1 | periodic_sig == -1;
periodic_sig = periodic_sig(indices);
t_vec = t_vec(indices);
angle_vec = angle_vec(indices);

function [indx_up, indx_down] = zeroCrossing(signal)
        x = diff(sign(signal));
        indx_up = find(x>0);
        indx_down = find(x<0);
end

freqs = 1 ./ diff(downsample(t_vec,4));

figure()
hold on
plot(t_vec, periodic_sig)
% plot(t_vec, angle_vec)
title('Fake periodic signal from Zero Crossings and Hilbert')

end