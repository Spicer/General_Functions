function cepstra = ccep(signal)
%CCEP Complex Cepstrum
% calculated according to https://en.wikipedia.org/wiki/Cepstrum

fft_signal = fft(signal);
phase = angle(fft_signal);
unwrapped_phase = unwrap(phase);
m = mod(unwrapped_phase, pi);
unwrapped_fft_signal = log(fft_signal) + 1j*2*pi*m;
cepstra = ifft(unwrapped_fft_signal, 'symmetric');

end