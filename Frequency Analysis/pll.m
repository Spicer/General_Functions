function out_sig = pll(t_vec, signal, sample_rate, pll_cf, filt_cutoff, filt_ord)
%PLL - Phase-locked loop
% This function attempts to follow an inout signal by using a phase-locked
% loop approach. Plots of error voltage and output and input vs time are
% presented
% Input arguments:
% t_vec is the time vector associated with the signal of interest
% signal is the signal of interest to be followed
% sample_rate is the rate at which the data was sampled
% pll_cf is the approximate center frequency of input
% filt_cutoff is a cutoff frequency, in Hz, for a low-pass filter to be
% applied to the signal before loop operation (this is optional)
% filt_ord is the order of the low-pass filter (this is optional)


% Pre-filtering:
if nargin > 4
    if nargin > 5
         b_pre = fir1(filt_ord, filt_cutoff/(sample_rate/2));
    else
        filt_ord = 9;
        b_pre = fir1(filt_ord, filt_cutoff/(sample_rate/2));
    end
    signal = filtfilt(b_pre, 1, signal);
end
% angle_vec = angle(hilbert(signal));

% Initialize values:
num_samples = size(signal, 1);
pll_integral_prev = 0;
% pll_lock = 0;
old_ref = 0;
pll_loop_gain = 15;
phase_detector_offset = (pi/5 + pi/2)*pll_loop_gain;
ref_sig = zeros(num_samples,1);
out_sig = zeros(num_samples,1);
pll_loop_control = zeros(num_samples,1);
pll_integral = zeros(num_samples,1);


% [b_loop, a_loop] = butter(1, 5*pll_cf / (0.5*sample_rate));
% [b_out, a_out] = butter(1, 0.006);

filt_l = 9;
delay = (filt_l - 1) / 2;
wn = 0.002;
b_loop = fir1(filt_l, wn);%,chebwin(35,30));%
% freqz(b_loop)

% H = dsp.FIRFilter;
% output_lowpass_freq = 1e9;
% siggy = sin(2*pi*1*t_vec);
% filt_siggy = filter(b_loop,1,siggy);
% figure()
% plot(t_vec(1:end-delay), filt_siggy(delay+1:end))
% hold on
% plot(t_vec, siggy)

for jj = 1+delay:num_samples
    samp = jj-delay;

    pll_loop_control(samp) = (signal(jj)*old_ref) * pll_loop_gain;
    if samp > num_samples / 100 && samp > 15
        pll_loop_control(samp) = loopLowpass(pll_loop_control(1:samp),'av');
    elseif samp > 15
        pll_loop_control(samp) = loopLowpass(pll_loop_control(1:samp),'av');
    end
%     output = outputLowpass(pll_loop_control);
    pll_integral(samp) = pll_integral_prev + pll_loop_control(samp) / sample_rate;
    ref_sig(samp) = cos(2 * pi * pll_cf * (t_vec(samp) + pll_integral(samp)));
    out_sig(samp) = cos(2 * pi * pll_cf * (t_vec(samp) + pll_integral(samp)) - ...
        phase_detector_offset);
    old_ref = ref_sig(samp);
    pll_integral_prev = pll_integral(samp);
end

figure()
subplot(211)
title('Error Voltage')
plot(t_vec, pll_loop_control)
xlabel('time [s]'); ylabel('error voltage')
subplot(212)
title('Integral of Error Voltage')
plot(t_vec, pll_integral)
xlabel('time [s]'); ylabel('error voltage')

% dada = abs(fft(pll_loop_control))/num_samples;
% freq = transpose(0:sample_rate/num_samples:sample_rate/2);
% figure()
% stem(freq, dada(1:floor(num_samples/2)+1))

% [s,f,t,ps] = plotSpectrogram(pll_loop_control, sample_rate);

    function output = outputLowpass(signal)
%         output = filtfilt(b_loop, a_loop, signal);
        output = filter(b_loop, 1, signal);
        output = output(end);
    end

    function signal = loopLowpass(signal, comm)
        if strcmp(comm, 'filt')
%             signal = filtfilt(b_out, a_out, signal);
            signal = filter(b_loop, 1, signal);
        else
            num_av = 15;
            cur_weight = 1;
            signal(end) = (sum(signal(end-num_av:end-1))+signal(end)*cur_weight)/...
                (num_av+cur_weight);
        end
        signal = signal(end);
    end

% angle_vec_ref = angle(hilbert(ref_sig));
% figure(); plot(t_vec, angle_vec_ref); hold on; plot(t_vec, angle_vec)
% legend('input', 'output')

% angle_diff = angle_vec - angle_vec_ref;

figure(); plot(t_vec, out_sig); hold on; plot(t_vec, signal); title('PLL Results')
xlabel('time [s]'); ylabel('voltage [V]')
legend('output', 'input')

% figure(); plot(out_sig); hold on; plot(ref_sig); title('PLL Results')
% legend('out', 'ref')
end