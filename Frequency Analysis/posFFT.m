function [freq, fft_data] = posFFT(data, sample_rate, window_length, window)

num_samples = size(data, 1);
if nargin < 3
    window_length = 2^nextpow2(num_samples);
    window = ones(size(data));
elseif nargin < 4
    window = ones(size(data));
else
    window = repmat(window(size(data, 1)), 1, size(data, 2));
end

data = window .* data;

freq = transpose(0:sample_rate/double(window_length):sample_rate/2);
fft_data = fft(data, window_length, 1)/num_samples;
fft_data = fft_data(1:floor(window_length/2)+1,:);
fft_data(2:end) = 2*fft_data(2:end);

end