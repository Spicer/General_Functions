function [freq, Gxx] = autospectrum(signal, sample_rate)

[freq, fft_data] = posFFT(signal, sample_rate, 0);
Gxx = conj(fft_data) .* fft_data;

end